﻿/* 
 * Solution For Dynamics 365 CRM Developer Test
 * Date : 8/Feb/2020
 * Author : Naveed Ahmed Alizai
 * Sources : https://docs.microsoft.com/
*/

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace CRM.Developer.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateRecords();
            Console.WriteLine("\nPress any key to exit...");
            Console.ReadKey(true);
        }

        static void CreateRecords()
        {
            try
            {
                string connectionString = "AuthType=" + ConfigurationManager.AppSettings["AuthType"] + ";" +
                    "Username=" + ConfigurationManager.AppSettings["Username"] + ";" +
                    "Password=" + ConfigurationManager.AppSettings["Password"] + ";" +
                    "Url=" + ConfigurationManager.AppSettings["Url"];
                CrmServiceClient client = new CrmServiceClient(connectionString);

                if (!client.IsReady)
                {
                    Console.WriteLine(client.LastCrmException.Message);
                    Console.WriteLine(client.LastCrmException.Source);
                    Console.WriteLine(client.LastCrmException.StackTrace);
                }
                else
                {
                    //5.When developing use something called "late binding" when accessing XRM
                    //6. Create two accounts and let the first one be the parent of the second one.

                    //Create parent account
                    Guid parentAccount1Id = CreateParentAccount(client);
                    Console.WriteLine($"Parent account created with id: {parentAccount1Id}");

                    //Create child account and associate to parent
                    Guid childAccount1Id = CreateChildAccount(client, parentAccount1Id);
                    Console.WriteLine($"Child account created with id: {childAccount1Id}");

                    //7. Create two contacts and associate the first one with the first account and the second one with the second account

                    //Create contact #1 and associate to parent
                    Guid contact1Id = CreateContact(client, "Contact #1", parentAccount1Id);
                    Console.WriteLine($"Contact #1 created with id: {contact1Id} and associated with Parent Account id : {parentAccount1Id}");

                    //Create contact#2 and associate to parent
                    Guid contact2Id = CreateContact(client, "Contact #2", childAccount1Id);
                    Console.WriteLine($"Contact #2 created with id: {contact2Id} and associated with Child Account with id : {childAccount1Id}");

                    //8.Update fields in one account and one contact
                    Entity accountToUpdate = new Entity(AccountEntity.EntityName, parentAccount1Id);
                    accountToUpdate.Attributes.Add(AccountEntity.MainPhone, "+37258156332");
                    client.Update(accountToUpdate);
                    Console.WriteLine($"Parent Account with id : {parentAccount1Id}, MainPhone field updated");

                    Entity contactToUpdate = new Entity(ContactEntity.EntityName, contact1Id);
                    contactToUpdate.Attributes.Add(ContactEntity.MobilePhone, "+37258156332");
                    contactToUpdate.Attributes.Add(ContactEntity.Email, "naveedahmed986@gmail.com");
                    OptionSetValue emailCode = new OptionSetValue((int)PreferedMethodOfContact.Email);
                    contactToUpdate.Attributes.Add(ContactEntity.PreferredMethodOfContact, emailCode);
                    client.Update(contactToUpdate);
                    Console.WriteLine($"Contact #1 with id : {contact1Id}, MobilePhone, Email and PreferredContactMethod fields updated");

                    //9.Create a note and associate it with the parent account 
                    Dictionary<string, CrmDataTypeWrapper> note = new Dictionary<string, CrmDataTypeWrapper>();
                    note.Add(AnnotationEntity.Title, new CrmDataTypeWrapper($"Note to Parent Account ", CrmFieldType.String));
                    note.Add(AnnotationEntity.Description, new CrmDataTypeWrapper($"Note description : this note is associated with Parent Account", CrmFieldType.String));
                    Guid noteToParentId = client.CreateAnnotation(AccountEntity.EntityName, parentAccount1Id, note);
                    Console.WriteLine($"Note with id : {noteToParentId} created and associated with Parent Account id : {parentAccount1Id}");

                    // 10. Create two notes and associate them with the second contact
                    note.Clear();
                    note.Add(AnnotationEntity.Title, new CrmDataTypeWrapper($"Note #1 to Contact #2", CrmFieldType.String));
                    note.Add(AnnotationEntity.Description, new CrmDataTypeWrapper($"Note #1 description : this note is associated with Contact #2", CrmFieldType.String));
                    Guid note1ToContact2Id = client.CreateAnnotation(ContactEntity.EntityName, contact2Id, note);
                    Console.WriteLine($"Note #1 with id : {note1ToContact2Id} created and associated with Contact #2 id : {contact2Id}");

                    note.Clear();
                    note.Add(AnnotationEntity.Title, new CrmDataTypeWrapper($"Note #2 to Contact #2", CrmFieldType.String));
                    note.Add(AnnotationEntity.Description, new CrmDataTypeWrapper($"Note #2 description : this note is associated with Contact #2", CrmFieldType.String));
                    Guid note2ToContact2Id = client.CreateAnnotation(ContactEntity.EntityName, contact2Id, note);
                    Console.WriteLine($"Note #2 with id : {note2ToContact2Id} created and associated with Contact #2 id : {contact2Id}");

                    //11.	Query the database for all contacts and all accounts and all notes. This should be done in one query. Create a list containing “name” (account or contact) and “notetext”.
                    PrintAllInfo(client);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static Guid CreateParentAccount(CrmServiceClient client)
        {
            Entity parentAccount = new Entity(AccountEntity.EntityName);
            parentAccount.Attributes.Add(AccountEntity.AccountName, "Parent Account");
            return client.Create(parentAccount);
        }

        static Guid CreateChildAccount(CrmServiceClient client, Guid parentAccountId)
        {
            Entity childAccount = new Entity(AccountEntity.EntityName);
            childAccount.Attributes.Add(AccountEntity.AccountName, "Child Account");
            childAccount.Attributes.Add(AccountEntity.ParentAccountId, new EntityReference(AccountEntity.EntityName, parentAccountId));
            return client.Create(childAccount);
        }

        static Guid CreateContact(CrmServiceClient client, string lastname, Guid parentAccountId)
        {
            Entity contact = new Entity(ContactEntity.EntityName);
            contact.Attributes.Add(ContactEntity.Lastname, lastname);
            contact.Attributes.Add(ContactEntity.CompanyName, new EntityReference(AccountEntity.EntityName, parentAccountId));
            return client.Create(contact);
        }

        static void PrintAllInfo(CrmServiceClient client)
        {
            //string fetchXml = @"<fetch mapping='logical' output-format='xml-platform' version='1.0' distinct='false'>
            //                      <entity name='annotation'>
            //                        <attribute name='notetext' />
            //                        <attribute name='annotationid' />
            //                        <link-entity name='account' to='objectid' from='accountid' alias='relatedaccount' link-type='outer' visible='false'>
            //                          <attribute name='name' />
            //                        </link-entity>
            //                        <link-entity name='contact' to='objectid' from='contactid' alias='relatedcontact' link-type='outer' visible='false'>
            //                          <attribute name='lastname' />
            //                        </link-entity>
            //                      </entity>
            //                    </fetch>";
            //EntityCollection records = client.RetrieveMultiple(new FetchExpression(fetchXml));

            // Alternative - using QueryExpression
            QueryExpression query = new QueryExpression();
            query.EntityName = AnnotationEntity.EntityName;
            query.ColumnSet = new ColumnSet();
            query.ColumnSet.Columns.Add(AnnotationEntity.Description);

            query.LinkEntities.Add(new LinkEntity(AnnotationEntity.EntityName, ContactEntity.EntityName,
                AnnotationEntity.ObjectId, ContactEntity.EntityId, JoinOperator.LeftOuter));
            query.LinkEntities[0].Columns.AddColumns(ContactEntity.Lastname);
            query.LinkEntities[0].EntityAlias = EntityAlias.RelatedContact;

            query.LinkEntities.Add(new LinkEntity(AnnotationEntity.EntityName, AccountEntity.EntityName,
                AnnotationEntity.ObjectId, AccountEntity.EntityId, JoinOperator.LeftOuter));
            query.LinkEntities[1].Columns.AddColumns(AccountEntity.AccountName);
            query.LinkEntities[1].EntityAlias = EntityAlias.RelatedAccount;

            query.Distinct = true;
            EntityCollection records = client.RetrieveMultiple(query);

            if (records != null && records.Entities != null && records.Entities.Count > 0)
            {
                foreach (var record in records.Entities)
                {
                    string noteText = record.Contains(AnnotationEntity.Description) ? (string)record[AnnotationEntity.Description] : "";

                    string contactName = record.Contains(EntityAlias.RelatedContact + "." + ContactEntity.Lastname) ?
                        (string)((AliasedValue)record[EntityAlias.RelatedContact + "." + ContactEntity.Lastname]).Value : "";

                    string accountName = record.Contains(EntityAlias.RelatedAccount + "." + AccountEntity.AccountName) ?
                        (string)((AliasedValue)record[EntityAlias.RelatedAccount + "." + AccountEntity.AccountName]).Value : "";

                    Console.WriteLine($"{noteText} \t {(string.IsNullOrEmpty(accountName) ? contactName : accountName)}");
                }
            }
            else
                Console.WriteLine("No records found");
        }
    }
    class AccountEntity
    {
        public static string EntityName = "account";
        public static string EntityId = "accountid";
        public static string AccountName = "name";
        public static string ParentAccountId = "parentaccountid";
        public static string MainPhone = "telephone1";
    }
    class ContactEntity
    {
        public static string EntityName = "contact";
        public static string EntityId = "contactid";
        public static string Lastname = "lastname";
        public static string CompanyName = "parentcustomerid";
        public static string MobilePhone = "mobilephone";
        public static string Email = "emailaddress1";
        public static string PreferredMethodOfContact = "preferredcontactmethodcode";
    }

    class AnnotationEntity
    {
        public static string EntityName = "annotation";
        public static string Title = "subject";
        public static string Description = "notetext";
        public static string ObjectId = "objectid";
    }

    class EntityAlias
    {
        public static string RelatedAccount = "relatedaccount";
        public static string RelatedContact = "relatedcontact";
    }

    enum PreferedMethodOfContact : int
    {
        Any = 1,
        Email = 2,
        Phone = 3,
        Fax = 4,
        Mail = 5
    }
}